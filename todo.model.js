const mongoose = require('mongoose');

const todoSchema = {
  title: String,
  completed: {
    type: Boolean,
    default: false
  },
  dueDate: Date
};

module.exports = mongoose.model('todo', todoSchema);
