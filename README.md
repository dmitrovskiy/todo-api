# Todo API

### Run locally

- npm i
- node app.js

OR

- Install docker & docker-compose
- docker-compose up --build

### GET - /todos

```
[
  {
    "_id": "mongo_id",
    "__v": 1,
    "title": "Order a book"
  }
]