FROM node:10.15-alpine

WORKDIR /var/www/html
COPY . .
RUN npm install

CMD node app.js
