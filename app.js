const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')

app.use(bodyParser.json());
app.use(cors());

mongoose.connect('mongodb://db:27017/todo', { useNewUrlParser: true });
const Todo = require('./todo.model');

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/todos', async function (req, res) {
  const todos = await Todo.find();
  res.json(todos);
});

app.post('/todos', async function (req, res) {
  const postData = req.body;
  const newTodo = new Todo(postData);
  await newTodo.save();

  res.json(newTodo);
});

app.get('/todos/:id', async function(req, res) {
  try {
    const todo = await Todo.findById(req.params.id);
    if(!todo) return res.status(404).json({ message: 'There is no todo with such id' });

    res.json(todo);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
});

app.patch('/todos/:id', async function(req, res) {
  try {
    await Todo.findByIdAndUpdate(req.params.id, req.body);

    res.status(204).send();
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
});

app.put('/todos/:id', async function(req, res) {
  try {
    const todo = await Todo.findById(req.params.id);
    if(!todo) return res.status(404).json({ message: 'There is no todo with such id' });

    Object.assign(todo, req.body);
    await todo.save();

    res.status(200).json(todo);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
});

app.delete('/todos/:id', async function(req, res) {
  try {
    const todo = await Todo.findByIdAndDelete(req.params.id);
    if(!todo) return res.status(404).json({ message: 'There is no todo with such id' });

    res.json(todo);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
});

app.listen(3000, function () {
  console.log('The app is listening on port 3000');
});
